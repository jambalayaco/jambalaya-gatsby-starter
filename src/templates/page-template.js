import React from "react"
import { Link, withPrefix } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

import matter from "gray-matter"
import remark from "remark"
import toHast from "mdast-util-to-hast"
import toHtml from "hast-util-to-html"

import "./page-template.css"

class PageTemplate extends React.Component {
  constructor(props) {
    super(props);
    const pathArray = String(this.props.data.markdownRemark.fileAbsolutePath).split("src");
    this.state = {
      isEdit: false,
      jamNeedsLoading: true,
      post: this.props.data.markdownRemark,
      title: this.props.data.markdownRemark.frontmatter.fields.title.value,
      html: this.props.data.markdownRemark.html,
      path: `src${pathArray[1]}`
    };
    this.toggleEdit = this.toggleEdit.bind(this);
    this.loadJam = this.loadJam.bind(this);
    this.onJambalayaEvent = this.onJambalayaEvent.bind(this);
  }

  componentWillUnmount() {
    if (typeof document !== 'undefined') {
      document.removeEventListener("jambalaya-event", this.onJambalayaEvent);
    }
  }

  toggleEdit() {
    if (this.state.jamNeedsLoading) {
      this.loadJam();
    }

    this.setState(state => ({
      isEdit: !state.isEdit,
    }));
  }

  loadJam() {

    const jam = document.createElement("script");
    jam.async = true;
    jam.src = withPrefix("/jam/jambalaya-editor.umd.js");
    this.div.appendChild(jam);

    var head  = document.getElementsByTagName('head')[0];
    var link  = document.createElement('link');
    link.rel  = 'stylesheet';
    link.type = 'text/css';
    link.href = withPrefix("/jam/jambalaya.css");
    link.media = 'all';
    head.appendChild(link);

    if (typeof document !== 'undefined') {
      document.addEventListener("jambalaya-event", this.onJambalayaEvent);
    }

    this.setState(state => ({
      jamNeedsLoading: false,
    }));

  };

  onJambalayaEvent(e) {
    const jamdata = e.detail;
    if (jamdata[this.state.path]) {

      const mat = matter(jamdata[this.state.path])
  
      const markdownAST = remark.parse(mat.content);
      const mhast = toHast(markdownAST)
      const html = toHtml(mhast);

      this.setState(state => ({
        title: mat.data.fields.title.value,
        html: html
      }));
    }
  }

  render() {
    return (
      <Layout>
        <div className={this.state.isEdit ? 'Editing' : null} >
          <div data-jambalaya={this.state.path} >
            <SEO title={this.state.title} />
            <h1 className="jambalaya-edit-icon">{this.state.title}</h1>
            <section dangerouslySetInnerHTML={{ __html: this.state.html }} />
          </div>
          <Link to="/">Go back to the homepage</Link>
          <button onClick={this.toggleEdit} className="editButton">
            {this.state.isEdit ? 
              <img src={withPrefix('/x.png')} alt="edit" />
              :
              <img src={withPrefix('/pencil.png')} alt="edit" />
            }
          </button>
        </div>

        <div id="jamwrapper" className={this.state.isEdit ? 'editorOpen' : 'editorClosed'} >
          <div id="jambalaya" data-project="18141144"></div>
          <div id="jambalayaEditor"></div> 
          <div id="jamscripts" ref={el => (this.div = el)}></div>
        </div>
      </Layout>
    );
  }
}

export default PageTemplate

export const pageQuery = graphql`
  query BlogPostBySlug($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      fileAbsolutePath
      id
      excerpt(pruneLength: 160)
      html
      rawMarkdownBody
      frontmatter {
        fields{
          title {
            value
          }
        }
      }
    }
  }
`
