import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

const IndexPage = ({ data }) => {
  const pages = data.allMarkdownRemark.nodes
  const pageList = pages.map((node) =>
    <li key={node.fields.slug}>
      <Link to={node.fields.slug} >
        Go to {node.frontmatter.fields.title.value}
      </Link>
    </li>
  );
  return (
  <Layout>
    <SEO title="Home" />
    <h1>Hi people</h1>
    <p>Welcome to your new Gatsby site.</p>
    <p>Now go build something great.</p>
    <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
      <Image />
    </div>
    <ul>
      {pageList}
    </ul>
  </Layout>
)}

export default IndexPage

export const pageQuery = graphql`
query MyQuery {
  allMarkdownRemark {
    nodes {
      fields {
        slug
      }
      frontmatter {
        fields{
          title {
            value
          }
        }
      }
    }
  }
}
`
